#!/bin/bash

STEAMPLAY_DIR="${HOME}/.steam/steam/steamapps/compatdata"

ELDEN_RING_PREFIX="${STEAMPLAY_DIR}/1245620/pfx"

PREFIX_ROOT="/drive_c/users/steamuser"
ELDEN_RING_SAVE_PATH="${PREFIX_ROOT}/AppData/Roaming/EldenRing/76561197971479138"

function backup_game {
  GAME_PREFIX="${ELDEN_RING_PREFIX}"
  GAME_SAVE_PATH="${ELDEN_RING_SAVE_PATH}"

  if [ -d "${GAME_PREFIX}/${GAME_SAVE_PATH}" ]; then
    echo
    echo "[INFO]: Attempting to back up Elden Ring save file..."
    ./backup_elden_ring_save_file.sh "${GAME_PREFIX}" "${GAME_SAVE_PATH}"
    RETURN_CODE=${?}

    if [ ${RETURN_CODE} -eq 0 ]; then
      echo
      echo "[SUCCESS]: Elden Ring save file successfully backed up!"
    else
      echo
      echo "[ERROR]: Something went wrong when attempting to back up Elden Ring save file. Exiting..."
      exit ${RETURN_CODE}
    fi

  else
    echo
    echo "[INFO]: Elden Ring save file does not exist."
  fi
}

# Backing up Elden Ring Remastered save file:
backup_game
RETURN_CODE=${?}

if [ ${RETURN_CODE} -eq 0 ]; then
  # Backing up all save-files remotely
  git add .. && git commit -m "Commit latest Elden Ring save game files." && git push
fi
