#!/bin/bash

STEAM_PREFIX="${HOME}/.steam/steam/steamapps/compatdata/335300/pfx"
PREFIX_ROOT="/drive_c/users/steamuser"

SOURCE_DIR="../save_files/dark_souls_2/DS2SOFS0000.sl2"
TARGET_DIR="${STEAM_PREFIX}${PREFIX_ROOT}/Application Data/DarkSoulsII/0110000100ab1a62"

echo
echo "[INFO]: Attempting to copy save-file from backup to game folder..."
cp "${SOURCE_DIR}" "${TARGET_DIR}"  
RETURN_CODE=${?}

if [ ${RETURN_CODE} -eq 0 ]; then
  echo
  echo "[SUCCESS]: Save-file copied from backup to game folder..."
else
  echo
  echo "[ERROR]: Problem copying save-file from backup to game folder..."
fi

exit ${RETURN_CODE}
